# Change Log
All notable changes to this project will be documented in this file.

#2016-09-08
- Fixed some errors in file-sort
- Removed/Added some files to delete in clean-homedir
- Added a redirect to update-git-repos
- Added a part in update-system to reset the amount of updates in ~/.updates
- Removed top panel because I suck at scripting and thought adding tonnes of cat executions was a good use of resources
- Fixed stuff in bspwmrc to reflect changes
- More repos in .mrconfig
- Change to a variable in init.vim
- Deleted some useless settings in plugin-settings.vim
- More compile flags added to keybinds.vim
- Sorted out plugins.vim
- Small settings.vim changes
- Added a tonne of compile flags to makefile template (A bit overkill,I know but i'll narrow it down over time)
- Fixed some stuff in sxhkdrc to reflect the removal of the top bar
- Added ibus stuff to .xinitrc
- Added update check to zshrc via cron running checkupdates to a file which zsh cats.
- Updated screenshots
- Added a file to .gitignore (netrwhist)

#2016-08-29
- More repos added to .mrconfig
- fixed make commands in nvim/keybinds.vim
- added more warning flags to nvim/plugin-settings.vim
- added more plugins in nvim/plugins.vim
- added folding options in nvim/settings.vim
- added nvim template files while removing the old ones
- some nvim template fixes
- added a script in applications/bin/ to set up projects (mkproject.sh)

#2016-08-28
- Updated my vim templates
- changed skel.h to skel.hpp
- added more repositories to mrconfig
- wrote out panel re-write plan and begun the rewrite by removing something useless in it.
- added cdpath variable to .zshrc to fix some error in vim,Will look into further

#2016-08-25
- Panel is kind of broken at the moment due to me screwing up some scripts,Looking into it
- No more stupid Alt text in Readme.md
- Reverted mrconfig split (was a stupid idea in the first place)
- Also reverted add-git-repo changes
- Switched over to neovim,Updated cleanhomedir to reflect this
- moved all vimconfigs to .config/nvim
- fixed some mistakes in filesort (will rewrite in c++ soon)

#2016-08-16
- Re-wrote ~/.bin/add-git-repo to seporate vim repos and general repos due to moving all my repos to an external hdd
- Fixed some stuff in file sort,Will do a re-write on it eventually
- Split mrconfig to .mrconfig which will hold all general repos i'm watching and .mrconfig-vim for all my vim repos
- Added ability to kill dusnt in sxhkd,I might make this it's own keybind later on.

#2016-08-10
- Completely re-wrote image-sort to include more filetypes (case/esac spam)
- Actually moved packages and packages-aur to resources (update-system kept piping the output in an untended place)
- more repos
- Small readme fixes

#2016-08-06
- Added folder for applications that require sudo
- Added crontab
- Change panel a bit to improve performance by not calling checkupdates every second

#2016-08-05
- Added filter to .gitignore to prevent a lock file being commited.
- More directories/wildcard and sorting sections were added to clean-homedir.
- Fixed tf2 section in bspwmrc.
- Added .global ignore in gitconfig.
- More repos added in mrconfig.
- Panel fixes + additions.
- Added ffmpeg thumbnail support in ranger (score.sh).
- Added more vim plugins.
- Removed all keybinds in .vim-settings and moved them into their own file .vim-keybinds.
- Added a script to sort images one by one under the name image-sort.
- Added a header file template for vim.
- Added a test script called chapter.sh it's purpose changes alot.
- Removed transparency files for TF2,We'll see how I manage without it.
- I changed the botton panel a bit so that it'll only alert me when the amount of updates is greater than 10,This is to prevent anything breaking.
- Renamed panel files to better explain their position on screen.


#2016-08-01
- Added newsbeuter back to the repo after a long hiatus.
- Also added newsbeuter screenshot.
- More repos added to .mrconfig.
- Added mouse support to vim and ranger.
- Removed blender screenshot due to it not being useful to really anyone but me,Blenders config will remain in the repo however.
- Fixed dunst being exactly one(!) pixel off from the top right.

#2016-07-31
- Fixed wildcards in clean-homedir.
- Updated colors in bspwmrc to match current colorscheme.
- Added more repos to mrconfig.
- Update/fixed colors in panel and panel_bar.
- Removed panel colors and added a global color file in ~/ (still experimental,will experiment futher).
- Ranger stuff (added quick bookmarks,Some changes in rc.conf and tags).
- added textsuggest usage to sxkdrc.
- Removed all the custom animations from tf2 due to valve blocking them.
- Added a few plugins to .vim-plugins.
- Added plugin configuration to .vim-plugin-settings.
- Updated .Xresources to match new colorscheme.
- Moved from oh-my-zsh to antigen.
- Updated zathurarc to match new colorscheme.
- Updated the screenshots to reflect new changes.
- Small changes in README.md

# 2016-07-22
- Added images of each application to show what it looks like.
- better diff support in .gitconfig via diff-so-fancy
- Fixed some rules in bspwmrc
- more repos added in mrconfig
- vim settings for both vim-plugins and vim-plugin-settings were changed/added
- updated packagelist

# 2016-07-19
- Added more rules to bspwmrc
- Added more repos to mrconfig
- Small vim changes

# 2016-07-15
- Added C++ template.
- Readded transparent viewmodels
- Commented out sed commands in noto-tf2-hud,Will fix sometime.
- Added games section to bspwmrc.
- Added input-ipc-server=/tmp/mpvsocket to mpv.conf for testing out SVP.
- More repos added to .mrconfig.
- Changed ranger configs around.
- Testing out using the numpad in sxhkd.
- small vim changes.

# 2016-07-09
- reworked/organised my clean-homedir script so it's not a huge mess of directories

# 2016-07-07
- Moved all applications to their own folder,"applications" for easier managing,I also updated the readme to reflect the changes with the relevant commands.
- Added a resource folder for stuff not entirely related to my dotfiles but still useful for me.
- Added vim templates folder at vim/.vim/templates
- Removed TF2 Hud + transparent viewmodels due to new update breaking them,Will re-add in the future.
- Added more to .vimrc-settings , .vimrc-plugins and .vimrc-plugin-settings

# 2016-07-06
- Removed demo folder due to me not realising how big demos actually are.
- added seperate .vim-plugin-settings to differentiate between vim settings and plugin settings.
- Completely re-did mpv config,Will tinker with it further.
- Removed Todo file
- added some more installed packages in dotfiles/packages
- cleared out ranger bookmarks to start anew.
- More repos were added to mrconfig.
- More plugins were added to vim-plugins and some more settings in .vim-settings
- Messed around with Readme.md and will continue to add to it
- Added a small message in zsh/.zshrc to remind me not to be a loafer.

# 2016-07-04
- Removed versioning in the changelog.Dotfiles don't need versioning (or a changelog for that matter)
- Removed kakoune config files
- Re-added vim config file base and will continue to work on it
- Added more repos in .mrconfig
- Updated Readme.md
- Added images of my desktop for demonstration purposes (Pretty silly of me not to have them in the first place)

# 2016-07-03
- Added a changelog
- Added proper TF2 demo commands and folders in anticipation for new HL season
- Edited ranger/.config/ranger/rc.conf to support git repos
