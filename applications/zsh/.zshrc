setopt extended_glob

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
. /usr/share/fzf/key-bindings.zsh


if [ $(cat $HOME/.updates | wc -l) -gt 0 ] ; then
	echo  -e "\033[32;5mUpdates available for:\033[0m"
	echo -e "$(cat $HOME/.updates)"
fi

#shuf -n 1 $HOME/todo

#Antigen
source $HOME/.antigen/antigen.zsh

antigen use oh-my-zsh
antigen bundle git
antigen bundle lein
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting
antigen theme flazz

antigen apply

#I'm lazy don't judge
eval $(keychain --eval --quiet id_rsa ~/.ssh/id_rsa)

#variables
export PANEL_FIFO PANEL_HEIGHT PANEL_FONT_FAMILY
export PATH="/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/home/lo/.panel:/home/lo/.bin:/usr/bin/core_perl"
export LANG=en_IE.UTF-8
export BSPWM_STATE=/tmp/bspwm-state.json
export EDITOR="/usr/bin/nvim"
export CDPATH="/home/lo"
export BROWSER="/usr/bin/firefox"
set EDITOR="nvim"
set TERMCMD="urxvt"
set SHELL="/bin/zsh"

#Aliases
alias clean-orphans="comm -23 <(pacman -Qqt | sort) <(echo $ignorepkg | tr ' ' '\n' | cat <(pacman -Sqg $ignoregrp) - | sort -u)"
alias rm="trash-put"
alias trash="trash-put"
alias empty="trash-empty"
alias tl="trash-list"
alias install="sudo pacman -S"
alias remove="sudo pacman -Rns"
alias orphan="sudo pacman -Rns $(pacman -Qtdq)"
alias clone="git clone"
