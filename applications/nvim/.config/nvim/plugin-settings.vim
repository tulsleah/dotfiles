"Ultisnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

"vim-clang
let g:clang_c_options = '-std=c++14 -Wall -g -pedantic-errors -Werror -Wextra -Wfatal-errors -Wold-style-cast -Wnon-virtual-dtor -Wshadow -Wmisleading-indentation -Wconversion -Woverloaded-virtual -Wcast-align -pthread'

"RainbowParentheses
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

"indent
let g:indent_guides_enable_on_vim_startup = 1
set background=dark
hi IndentGuidesOdd  ctermbg=black
hi IndentGuidesEven ctermbg=darkgrey

"colorizer
let g:colorizer_startup = 1

"NerdTree
let NERDTreeShowHidden=1

"vim-cpp-enhanced-highlight
let g:cpp_class_scope_highlight = 1

"deoplete
let g:deoplete#enable_at_startup = 1

"auto-pairs
let g:AutoPairsFlyMode = 0

