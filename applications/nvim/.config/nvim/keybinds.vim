let mapleader=" "

"make
map <leader>m :make -C ../<CR>
map <leader>r :make -C ../ run<CR>

"compiling
map <leader>b :!clang++ -std=c++14 -fsanitize=undefined,return,null,bool,float-divide-by-zero,unsigned-integer-overflow -Weverything -Wno-c++98-compat -pthread % && ./a.out <CR>
map <leader>c :ClangCloseWindow<CR>

"VimTab stuff if you're into that sort of stuff
map <S-Left> :tabprevious<CR>
map <S-Right> :tabnext<CR>

"gundo
map <leader>g :GundoToggle<CR>

"tagbar
map <leader>t :TagbarToggle<CR>

"NerdTree
map <leader>n :NERDTreeToggle<CR>

