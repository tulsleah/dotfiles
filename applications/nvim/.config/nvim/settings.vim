syntax enable

colorscheme zenburn

set showmode
set autoindent
set cursorline
set number
set exrc
set secure
set scrolloff=5
set mouse=a
set switchbuf=usetab

"Folowing linux kernel style
set tabstop=8
set softtabstop=8
set shiftwidth=8

set noexpandtab
set wildmenu
set showcmd
set ruler
set confirm
set undodir=$HOME/.vim/undo
set wildignore=*.o,*~,*.pyc,*.out
set undolevels=10000
set undoreload=10000
set autoread
set noswapfile
set clipboard=unnamed

"au BufWinLeave ?* mkview
"au BufWinEnter ?* silent loadview

autocmd! BufWritePost *.cpp ClangSyntaxCheck
autocmd! Bufwritepost *.vim source %


augroup resCur
  autocmd!
  autocmd BufReadPost * call setpos(".", getpos("'\""))
augroup END"'"))

let vimDir = '$HOME/.vim'
let &runtimepath.=','.vimDir

if has('persistent_undo')
	let myUndoDir = expand(vimDir . '/undodir')
	call system('mkdir ' . vimDir)
	call system('mkdir ' . myUndoDir)
	let &undodir = myUndoDir
	set undofile
endif

