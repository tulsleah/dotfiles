#!/bin/bash

dir=/var/run/media/lo/Doujins

find . -type f -name '*.zip' -print0 | while read -d $'\0' file
do
    if [[ ! -f "$file" ]] ; then continue; fi
    #echo "$file"
    unzip -t -qq "$file"
    RETVAL=$?
    case $RETVAL in
        # 0=success, archive file is okay
        0)
            echo "OK: $file"
            mv "$file" $dir/fine
            ;;
        3)
            echo "BROKEN($RETVAL): $file"
            mv "$file" $dir/broken
            ;;
        9)
            echo "WRONGFORMAT($RETVAL): $file"
            mv "$file" "$file.wrongformat"
            ;;
        80)
            echo "USER ABORT: $file"
            ;;
        81)
            echo "ENCRYPTED($RETVAL): $file"
            mv "$file" "$file.encrypted"
            ;;
        82)
            echo "PASSWORD($RETVAL): $file"
            mv "$file" "$file.password"
            ;;
        *)
            echo "ERROR($RETVAL): $file"
            ;;
    esac
done

find . -type f -name '*.rar' -print0 | while read -d $'\0' file
do
     if [[ ! -f "$file" ]] ; then continue; fi
     unrar t "$file"
     RETVAL=$?
     case $RETVAL in
         0)
             echo "OK: $file"
             mv "$file" $dir/fine
             ;;
         10)
             echo "WRONGFORMAT($RETVAL): $file"
             ;;
         3)
             echo "BROKEN($RETVAL): $file"
             mv "$file" $dir/broken
             ;;
     esac
done

find . -type f -name '*.tar.gz' -print0 | while read -d $'\0' file
do
    if [[ ! -f "$file" ]] ; then continue; fi
    gunzip -t $file
    RETVAL=$?
    case $RETVAL in
        0)
            echo "OK: $file"
            mv "$file" $dir/fine
            ;;
        1)
            echo "BROKEN: $file"
            mv "$file" "$dir/broken/"
            ;;
    esac
done
