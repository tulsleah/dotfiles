#!/usr/bin/env bash

set -o errexit

ESC_SEQ="\x1b["
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
chapter=$1

if [[ -z "$1" ]]
then
        echo -e "$COL_RED"Error"$COL_RESET"
        echo "Usage: chapter [number.number]"

        exit 1
fi

echo "Is $1 correct?"
echo -e "["$COL_GREEN"yes"$COL_RESET"/"$COL_RED"no"$COL_RESET"]"
read answer
if [ "$answer" == "yes" ]
then
	echo "$1" > $HOME/.current-chapter
else
	if [ "$answer" == "no" ]
	then
		:
	fi
fi
