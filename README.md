I use GNU Stow to manage my dotfiles,It should be in your distro repositories so just install it from there if you want.


	#Ubuntu/Debian
	sudo apt-get install stow
	#Arch
	sudo pacman -S stow
	#Fedora
	sudo dnf install stow

	etc

then clone the repo with the following:

	git clone https://gitlab.com/tulsleah/dotfiles.git dotfiles && cd dotfiles/applications/
or

	git clone https://github.com/tulsleah/dotfiles dotfiles && cd dotfiles/applications/

And from there you can use stow to get the configs for whatever application you want.

	stow -t $HOME vim/
	stow -t $HOME bspwm/
	stow -t $HOME sxhkd/
	stow -t $HOME panel/

	#and so on...

or if you want all of the configs you can run:

	cd $HOME/dotfiles/applications/
	stow -t $HOME *

If you run into any errors when stowing,it usually means you already have configs in that location and stow wont overwrite them so don't worry just rename/move/backup your old configs and it should work then.

To remove all of the config symlinks you can run:

	cd $HOME/dotfiles/applications/
	stow -t $HOME -D *

Applications that require elevated permissions (such as /var/spool/cron/) are located in $HOME/dotfiles/sudo,To use these just enter the following:

	cd $HOME/dotfiles/sudo/
	sudo stow -t / cron/

I'm pretty sure these configs are only useful to me but i'll still document them here just in case.

Most of these configs should be platform agnostic,But keep in mind I use archlinux so some stuff may not work on other systems without editing them a bit,
Which is what you should be doing anyway.
---
Here's what everything looks like!

Clean desktop

![Clean](resources/screenshots/desktop-clean.png)

and messy

![Messy](resources/screenshots/desktop-messy.png)

---

ncmpcpp

![ncmpcpp](resources/screenshots/ncmpcpp.png)

---

dunst

![dunst](resources/screenshots/dunst.png)

---

mpv

![mpv](resources/screenshots/mpv.png)

---

ranger

(Pretty much default)

![ranger](resources/screenshots/ranger.png)

---

rtorrent

![rtorrent](resources/screenshots/rtorrent.png)

---

nvim

![neovim](resources/screenshots/vim.png)

---

zathura

(Not much to see,Which is kinda the point of a document viewer)

![zathura](resources/screenshots/zathura.png)

---

newsbeuter

![newsbeuter](resources/screenshots/newsbeuter.png)

---

zsh

![zsh](resources/screenshots/zsh.png)
